;;; Configuration of the guix-hpc8 build node.

(use-modules (gnu))

(let ((base-os (load "build-node.scm")))
  (operating-system
    (inherit base-os)
    (host-name "guix-hpc8")

    ;; This is a UEFI setup.
    (bootloader (bootloader-configuration
                  (bootloader grub-efi-bootloader)
                  (targets (list "/boot/efi"))))

    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))

    (file-systems (append (list (file-system
                                  (mount-point "/boot/efi")
                                  (device (uuid "B65C-D597" 'fat32))
                                  (type "vfat"))
                                (file-system
                                  (mount-point "/")
                                  (device (uuid "af2de248-a752-4a49-b391-fb471fe913f6"
                                                'ext4))
                                  (type "ext4")))
                          %base-file-systems))

    (swap-devices (list (swap-space
                         (target (uuid "278cd11b-7ff2-4b75-9138-03dcd69a911c")))))

    (services
     ;; Configure networking.
     (cons  (service static-networking-service-type
                     (list (static-networking
                            (addresses (list (network-address
                                              (device "ens10f0")
                                              (value "194.199.1.24/27"))))
                            (routes (list (network-route
                                           (destination "default")
                                           (gateway "194.199.1.30"))))
                            (name-servers '("193.50.111.150")))))

            (operating-system-user-services base-os)))))
