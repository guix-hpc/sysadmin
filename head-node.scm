;;; System configuration of "guix-hpc4", the Cuirass head node.
;;;
;;; This module is part of Guix-HPC and is licensed under the same terms,
;;; those of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2017-2025 Inria

(use-modules (gnu)
             ((guix store) #:select (%store-prefix))
             (guix packages)
             (guix modules)
             ((guix utils) #:select (substitute-keyword-arguments))
             (srfi srfi-1)
             (srfi srfi-26)
             (ice-9 match))
(use-service-modules admin avahi certbot cuirass databases linux
                     mcron networking
                     shepherd ssh virtualization web)
(use-package-modules certs databases screen ssh web)
(use-modules ((cuirass-package) #:prefix latest:))

(define cuirass-latest/debug
  (package/inherit latest:cuirass
    (arguments
     (substitute-keyword-arguments (package-arguments latest:cuirass)
       ((#:phases phases #~%standard-phases)
        #~(modify-phases #$phases
            (add-after 'strip 'enable-debug-logging
              (lambda* (#:key outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                  (substitute* (string-append out "/bin/cuirass")
                    (("^exec .*" all)
                     (string-append "\
export CUIRASS_LOGGING_LEVEL=${CUIRASS_LOGGING_LEVEL:-debug}\n"
                                    all))))))))))))

(define %custom-base-services
  (modify-services %base-services
    (guix-service-type config =>
                       (guix-configuration
                        (inherit config)
                        (use-substitutes? #t)
                        (authorized-keys
                         ;; Authorize substitutes from Ludo's laptop to allow
                         ;; for offloading.
                         (append (list (local-file "./ribbon-export.pub")
                                       (local-file "guix/keys/guix-hpc1.pub")
                                       (local-file "guix/keys/guix-hpc3.pub")
                                       (local-file "guix/keys/guix-hpc4.pub")
                                       (local-file "guix/keys/guix-hpc5.pub")
                                       (local-file "guix/keys/guix-hpc6.pub")
                                       (local-file "guix/keys/guix-hpc7.pub")
                                       (local-file "guix/keys/guix-hpc8.pub"))
                                 (guix-configuration-authorized-keys config)))
                        (build-accounts 60)

                        ;; We have 24 cores.
                        (extra-options '("--max-jobs=2" "--cores=6"))))))

;; TODO: Share with 'build-node.scm'.
(define (garbage-collector-service name calendar free-space)
  "Return a Shepherd timer called NAME that collects garbage according to
CALENDAR, a gexp, and ensures at least FREE-SPACE GiB are available."
  (simple-service
   name shepherd-root-service-type
   (list (shepherd-service
          (provision (list name))
          (requirement '(user-processes))
          (modules '((shepherd service timer)))
          (start #~(make-timer-constructor
                    #$calendar
                    (command '("/run/current-system/profile/bin/guix" "gc"
                               "-F" #$(format #f "~aG" free-space)
                               "-d" "2m"))
                    #:wait-for-termination? #t))
          (stop #~(make-timer-destructor))
          (documentation "Run the garbage collector (GC).")
          (actions
           (list (shepherd-action
                  (name 'trigger)
                  (documentation "GC!")
                  (procedure #~trigger-timer))))))))

(define %garbage-collector-services
  ;; Collect garbage several times a day, with bigger collections twice a
  ;; day.
  (list (garbage-collector-service 'gc-quick
                                   #~(calendar-event #:minutes '(30)
                                                     #:hours '#$(iota 24))
                                   20)
        (garbage-collector-service 'gc
                                   #~(calendar-event #:minutes '(0)
                                                     #:hours '(5 15))
                                   60)))

(define (guix-channel branch)
  #~(channel
     (name 'guix)
     (url "https://git.savannah.gnu.org/git/guix.git")
     (branch #$branch)
     (introduction
      (make-channel-introduction
       "9edb3f66fd807b096b48283debdcddccfea34bad"
       (openpgp-fingerprint
        "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

(define cuirass-specs
  ;; Spec to build packages from the 'guix', 'guix-hpc', and 'guix-past'
  ;; channels against different branches of Guix.
  ;; Note about priorities: 9 is lowest, 1 is highest.
  #~(list (specification
           (name 'nonguix)
           (build '(packages "firefox" "linux"))
           (priority 7)
           (channels
            (cons (channel
                   (name 'nonguix)
                   (url "https://gitlab.com/nonguix/nonguix")
                   (introduction
                    (make-channel-introduction
                     "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                     (openpgp-fingerprint
                      "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
                  %default-channels)))

          (specification
           (name 'guix-science)
           (priority 3)
           (build '(channels guix-science))
           (channels
            (cons (channel
	           (name 'guix-science)
	           (url "https://codeberg.org/guix-science/guix-science.git")
	           (introduction
	            (make-channel-introduction
		     "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
		     (openpgp-fingerprint
		      "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
                  %default-channels)))

          (specification
           (name 'guix-science-nonfree)
           (priority 5)
           (build '(channels guix-science-nonfree))
           (channels
            (cons (channel
	           (name 'guix-science-nonfree)
	           (url "https://codeberg.org/guix-science/guix-science-nonfree.git")
	           (introduction
                    (make-channel-introduction
                     "58661b110325fd5d9b40e6f0177cc486a615817e"
                     (openpgp-fingerprint
                      "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
                  %default-channels)))

          (specification
           (name 'guix-cran)
           (priority 8)
           (build '(channels guix-cran))
           (period (* 20 60))
           (channels
            (cons (channel                        ;unauthenticated
                   (name 'guix-cran)
                   (url "https://github.com/guix-science/guix-cran.git"))
                  %default-channels)))

          (specification
           (name 'guix-bioc)
           (priority 8)
           (build '(channels guix-bioc))
           (period (* 20 60))
           (channels
            (cons (channel                        ;unauthenticated
                   (name 'guix-bioc)
                   (url "https://github.com/guix-science/guix-bioc.git"))
                  %default-channels)))

          #$@(map (lambda (name branch priority period)
                    #~(specification
                       (name '#$name)
                       (priority #$priority)
                       (period #$period)
                       (build '(manifests "manifest"))
                       (channels
                        (list (channel
                               (name 'guix-hpc-non-free)
                               (url
                                "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git"))
                              #$(guix-channel branch)))))
                  '(guix-hpc-non-free)
                  '("master")
                  '(4)                          ;priority
                  `(0))                         ;period

          #$@(map (lambda (name branch priority period)
                    #~(specification
                       (name '#$name)
                       (priority #$priority)
                       (period #$period)
                       (build '(channels guix-hpc))
                       (channels
                        (list (channel
                               (name 'guix-hpc)
                               (url
                                "https://gitlab.inria.fr/guix-hpc/guix-hpc.git"))
                              #$(guix-channel branch)))))
                  '(guix-hpc)
                  '("master")
                  '(1)                          ;priority
                  `(0))                         ;period

          (specification
           (name 'guix-hpc-power9)
           (systems '("powerpc64le-linux"))
           (priority 9)
           ;; Build some of the dependencies of GEOS.
           (build '(packages "camp" "blt" "hdf5-geos" "conduit" "silo"
                             "raja" "chai" "adiak" "caliper"))
           (period (* 60 60))
           (channels
            (list (channel
                   (name 'guix-hpc)
                   (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git"))
                  #$(guix-channel "master"))))

          #$@(map (lambda (name branch)
                    #~(specification
                       (name '#$name)
                       (priority 4)
                       (build '(channels guix-past))
                       (channels
                        (list (channel
                               (name 'guix-past)
                               (url "https://codeberg.org/guix-science/guix-past.git")
                               (introduction
                                (make-channel-introduction
                                 "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
                                 (openpgp-fingerprint
                                  "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
                              #$(guix-channel branch)))))
                  '(guix-past guix-past-1.4.0)
                  '("master" "version-1.4.0"))

          #$@(map (lambda (name branch)
                    #~(specification
                       (name '#$name)
                       (priority 1)
                       (period #$(if (string=? branch "master")
                                     (* 5 60)
                                     (* 12 60 60)))
                       (build '(packages "gcc-toolchain" "openmpi" "mpich"
                                         "hwloc" "intel-mpi-benchmarks"
                                         "petsc" "slepc"
                                         "petsc-openmpi" "slepc-openmpi"
                                         "python-scipy" "scotch" "pt-scotch"
                                         "metis" "mumps" "mumps-metis"
                                         "mumps-metis-openmpi" "mumps-openmpi"
                                         "slurm" "vtk" "emacs" "emacs-org"
                                         "jupyter" "guix-jupyter" "julia"))
                       (channels (list #$(guix-channel branch)))))
                  '(guix guix-1.4.0)
                  '("master" "version-1.4.0"))

          (specification
           (name 'images)
           (systems '("x86_64-linux"))
           (channels (cons (channel
                            (name 'guix-images)
                            (url "https://gitlab.inria.fr/numpex-pc5/wp3/guix-images")
                            (branch "main"))
                           %default-channels))
           (build '(manifests "manifests/main.scm"))
           (build-outputs
            (list (build-output
                   (job ".*-image")
                   (type "archive")
                   (path ""))))
           (period 86400)
           (priority 4))))

(define this-file
  ;; This configuration file next to its companion files (SSH keys, nginx
  ;; config snippets, etc.)
  (file-append (local-file "." "os-config"
                           #:recursive? #t
                           #:select?
                           ;; Note: Don't use 'git-predicate' as this
                           ;; wouldn't work for unattended-upgrade, where the
                           ;; copy of this directory in the store is not a
                           ;; Git checkout.
                           (lambda (file stat)
                             (and (not (string=? (basename file) ".git"))
                                  (or (eq? 'directory (stat:type stat))
                                      (any (cut string-suffix? <> file)
                                           '("scm" "conf" "pub" "org"))))))
               "/head-node.scm"))


;;;
;;; Disk space watchdog.
;;;

(define disk-space-check
  ;; Check disk space on the store and on the root file system; stop the
  ;; 'cuirass' service if disk space is too low.
  (match-lambda
    ((low-store low-root)
     (program-file "check-disk-space"
                   (with-imported-modules (source-module-closure
                                           '((gnu services herd)
                                             (guix build syscalls)))
                     #~(begin
                         (use-modules (gnu services herd)
                                      (guix build syscalls))

                         (when (or (< (free-disk-space #$(%store-prefix))
                                      #$low-store)
                                   (< (free-disk-space "/")
                                      #$low-root))
                           (format #t "Low disk space, stopping Cuirass!~%")
                           (stop-service 'cuirass))))))))

(define (disk-space-shepherd-services thresholds)
  (list (shepherd-service
         (requirement '(user-processes cuirass))
         (provision '(disk-space-watchdog))
         (modules '((shepherd service timer)))
         (start #~(make-timer-constructor
                   (calendar-event #:minutes '(0)) ;once per hour
                   (command '(#$(disk-space-check thresholds)))))
         (stop #~(make-timer-destructor))
         (documentation "Stop Cuirass when disk space is too low.")
         (actions (list (shepherd-action
                         (name 'trigger)
                         (documentation "Check!")
                         (procedure #~trigger-timer)))))))

(define disk-space-watchdog-service-type
  (service-type (name 'disk-space-watchdog)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          disk-space-shepherd-services)))
                (description "Stop Cuirass when disk space is too low.")))

(define GiB (expt 2 30))


;;;
;;; NGINX.
;;;

(define %nginx-config
  (computed-file "nginx-config"
                 (with-imported-modules
                  '((guix build utils))
                  #~(begin
                      (use-modules (guix build utils))

                      (mkdir #$output)
                      (chdir #$output)
                      (symlink #$(local-file "nginx-config/nginx.conf")
                               "nginx.conf")
                      (copy-file #$(local-file
                                    "nginx-config/nginx-locations.conf")
                                 "nginx-locations.conf")))))

(define %nginx-gitlab-token
  ;; Create /etc/nginx-tokens with a random token if it doesn't exist.
  (simple-service 'nginx-gitlab-token
                  activation-service-type
                  #~(begin
                      (define file "/etc/nginx-tokens")
                      (define (make-random-list n)
                            (if (= n 0)
                                '()
                                (cons (integer->char
                                       (+ (random 26) 97)) ;; ASCII code for a-z
                                      (make-random-list (- n 1)))))
                      ;; token is a random lowercase alphabetical
                      ;; string containing 32 characters. This
                      ;; corresponds to 1.90172e+45 different
                      ;; combinations.
                      (define token
                        (list->string (make-random-list 32)))

                      (unless (file-exists? file)
                        (call-with-output-file file
                          (lambda (port)
                            (format port "set $GITLAB_TOKEN ~a;~%" token)
                            ;; Ensure that the file is only readable by its owner.
                            (chmod port #o600)))))))

(define %nginx-mime-types
  ;; Provide /etc/nginx/mime.types (and a bunch of other files.)
  (simple-service 'nginx-mime.types
                  etc-service-type
                  `(("nginx" ,(file-append nginx "/share/nginx/conf")))))

(define %nginx-deploy-hook
  ;; Hook that restarts nginx when a new certificate is deployed.
  (program-file "nginx-deploy-hook"
                #~(execl #$(file-append nginx "/sbin/nginx")
                         "nginx" "-s" "reload")))


(operating-system
  (host-name "guix-hpc4")
  (timezone "Europe/Paris")
  (locale "fr_FR.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))

  ;; sudo mdadm --create /dev/md0 --level raid10 --raid-disks 4 -p f2 /dev/sd[ab]2

  ;; Add a kernel module for RAID-10.
  (initrd-modules (cons "raid10" %base-initrd-modules))

  (mapped-devices (list (mapped-device
                         (source (list "/dev/sda2" "/dev/sdb2"))
                         (target "/dev/md0")
                         (type raid-device-mapping))))

  (file-systems (append (list (file-system
			        (device (uuid "13C4-01C8" 'fat32))
			        (mount-point "/boot/efi")
			        (type "vfat"))
                              (file-system
                                (device "/dev/md0")
                                (mount-point "/")
                                (type "ext4")
                                (dependencies mapped-devices)))
                        %base-file-systems))

  (users (append (load "admins.scm") %base-user-accounts))

  ;; Globally-installed packages.
  (packages (cons* screen
                   openssh                  ;so that 'scp' works
                   %base-packages))

  (services
   (append (list (service static-networking-service-type
                          (list (static-networking
                                 (addresses
                                  (list (network-address
                                         (device "eno1")
                                         (value "194.199.1.1/27"))
                                        (network-address
                                         (device "eno1")
                                         (value "2001:660:6102:101::1/64"))))
                                 (routes
                                  (list (network-route
                                         (destination "default")
                                         (gateway "194.199.1.30"))
                                        (network-route
                                         (destination "default")
                                         (gateway "2001:660:6102:101::30"))))
                                 (name-servers
                                  '("193.50.111.150")))))

                 (service openssh-service-type
                          (openssh-configuration
                           (port-number 22)

                           ;; Allow root login with public key only to permit use
                           ;; of 'guix deploy'.
                           (permit-root-login 'prohibit-password)

                           (authorized-keys
                            `(("ludo" ,(local-file "ludo.pub"))
                              ("florent" ,(local-file "florent.pub"))
                              ("eagullo" ,(local-file "manu.pub"))
                              ("jlelaura" ,(local-file "julien.pub"))
			      ("rgarbage" ,(local-file "romain.pub"))
                              ("root" ,(local-file "ludo.pub")
                                      ,(local-file "romain.pub"))))))

                 (service unattended-upgrade-service-type
                          (unattended-upgrade-configuration
                           (operating-system-file this-file)
                           (services-to-restart
                            '(mcron nginx guix-daemon qemu-binfmt tor unattended-upgrade))
                           (channels
                            #~(cons* (channel
                                      (name 'cuirass)
                                      (url "https://git.savannah.gnu.org/git/guix/guix-cuirass.git")
                                      (branch "main")
                                      (introduction
                                       (make-channel-introduction
                                        "c75620777c33273fcd14261660288ec1b2dc8123"
                                        (openpgp-fingerprint
                                         "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
                                     %default-channels))))

                 (service nginx-service-type
                          (nginx-configuration
                           (file (file-append %nginx-config
                                              "/nginx.conf"))))
                 %nginx-mime-types
                 %nginx-gitlab-token

                 (service certbot-service-type
                          (certbot-configuration
                           (webroot "/var/www")
                           (email "ludovic.courtes@inria.fr")
                           (certificates
                            (list (certificate-configuration
                                   (domains '("guix.bordeaux.inria.fr"))
                                   (deploy-hook %nginx-deploy-hook))))))

                 (service guix-publish-service-type
                          (guix-publish-configuration
                           (port 3000)
                           (compression `(("zstd" 19)))
                           (cache "/var/cache/guix/publish")
                           ;; Note: That's ~30 GiB/month.
                           (ttl (* 12 30 24 3600))  ;12 months
                           (cache-bypass-threshold (* 100 (expt 2 20))))) ;100 MiB

                 (service ntp-service-type)
                 (service qemu-binfmt-service-type
                          (qemu-binfmt-configuration
                           (platforms (lookup-qemu-platforms "arm"
                                                             "aarch64"))))

                 (service cuirass-service-type
                          (cuirass-configuration
                           (cuirass cuirass-latest/debug)

                           ;; By default, poll repositories relatively
                           ;; infrequently.  Send POST requests on
                           ;; /jobset/NAME/hook/evaluate when faster feedback
                           ;; is needed.
                           (interval 300)

                           ;; Offload builds via the "remote worker"
                           ;; mechanism.
                           (remote-server
                            (cuirass-remote-server-configuration
                             (trigger-url "http://localhost:3000")))

                           (specifications cuirass-specs)))

                 ;; Increase 'max_connections' so Cuirass gets enough.
                 ;; Default is "typically 100" according to
                 ;; <https://www.postgresql.org/docs/current/runtime-config-connection.html>.
                 (service postgresql-service-type
                          (postgresql-configuration
                           ;; See
                           ;; <https://www.postgresql.org/docs/current/upgrading.html>
                           ;; on how to upgrade the database when migrating
                           ;; to a newer version of PostgreSQL.
                           (postgresql postgresql-15)
                           (config-file
                            (postgresql-config-file
                             (extra-config
                              '(("max_connections" 300)))))))

                 (service avahi-service-type)     ;for cuirass-remote-server

                 ;; Don't let bogus jobset evaluations put the machine on its
                 ;; knees.
                 (service earlyoom-service-type)

                 ;; Stop Cuirass when disk space is low.
                 (service disk-space-watchdog-service-type
                          (list (* 1 GiB) (* 1 GiB))))

           %garbage-collector-services
           %custom-base-services)))
