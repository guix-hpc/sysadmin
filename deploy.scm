;;; This file is for use by 'guix deploy'.

;; Here we assume that 'guix-hpc4' etc. are defined in the user's
;; ~/.ssh/config.  See 'README.org' for more information.

(use-modules (gnu machine ssh)
             ((guix ui) #:select (load* make-user-module)))

(define (operating-system-configuration-module)
  ;; Module in which the machine description file is loaded.
  (make-user-module '((gnu system)
                      (gnu services)
                      (gnu system shadow))))

(define (load-operating-system file)
  (load* file (operating-system-configuration-module)))

(list (machine                                 ;the head node, aka. guix-hpc4
       (operating-system (load-operating-system "head-node.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc4")
                       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIItVWW6JlDQ21Bnfz9A7Z105i6kORrXDPfyNaf4uj0rm root@(none)"))))

      ;; Build nodes.

      (machine
       (operating-system (load-operating-system "guix-hpc1.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc1")
                       (host-key
                        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP0yTo+S20dXf0ep0AC7gJ018cL8dsBTZ/z0QkLc9LC7 root@(none)"))))


      (machine
       (operating-system (load-operating-system "guix-hpc3.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc3")
                       (host-key
                        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHZQz08uAzQu3+eo4RbDi/Ost/M5wgUihuidfQELM+F5 root@(none)"))))


      (machine
       (operating-system (load-operating-system "guix-hpc5.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc5")
                       (host-key
                        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC3yWg6cbs8xDR43e/LEMRYpsDInRCUdtytliSMOhxjQ root@(none)"))))
      (machine
       (operating-system (load-operating-system "guix-hpc6.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc6")
                       (host-key
                        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG2wXmXULCOeYKanz+HuHYRfv1SU/EEZ5/E+0Hy1eCl0 root@(none)"))))

      (machine
       (operating-system (load-operating-system "guix-hpc7.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc7")
                       (host-key
                        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOyhK3PjJ1aKtkOHoBUke0dqKAtBbh1rqAh9W7W0qLRp root@(none)"))))

      (machine
       (operating-system (load-operating-system "guix-hpc8.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (system "x86_64-linux")
                       (host-name "guix-hpc8")
                       (host-key
                        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIES0cHOOh1xBXj/Yi2DNVju1k12utBal5Ut3hBAHUu6R root@(none)")))))
