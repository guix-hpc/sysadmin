;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2023-2025 Inria

;;; This manifest is used to generate environment modules for the PlaFRIM
;;; cluster.  Create or update modules with:
;;;
;;;   guix shell guix guix-modules -- \
;;;     guix time-machine -C channels.scm -- \
;;;     module create -m manifest.scm -o /cm/shared/dev/modules/guix

(use-modules (gnu packages)
             (guix packages)
             (guix diagnostics)
             (guix profiles)
             (guix describe)
             (srfi srfi-26))

(define (categorize p category)
  "Mark P as belonging to CATEGORY."
  (package
    (inherit p)
    (properties `((module-category . ,category)
                  ,@(package-properties p)))))

(define (manifest-entry-with-category entry category)
  "Return ENTRY with an additional 'module-category' property if its item is
a package."
  (manifest-entry
    (inherit entry)
    (dependencies (map manifest-entry-with-category
                       (manifest-entry-dependencies entry)))
    (properties
     (let ((item (manifest-entry-item entry)))
       (if (package? item)
           `((module-category . ,category)
             ,@(manifest-entry-properties entry))
           (manifest-entry-properties entry))))))

(define toolchains
  (map-manifest-entries
   ;;(cut manifest-entry-with-category <> "compiler")
   identity
   (specifications->manifest
    '("gcc-toolchain@13" "gcc-toolchain@14"
      "gcc-toolchain@12" "gcc-toolchain@11"
      "gcc-toolchain@10" "gcc-toolchain@9"
      "gfortran-toolchain"
      "clang-toolchain@17" "clang-toolchain@18"
      "clang-toolchain@15" "clang-toolchain@16"
      "clang-toolchain@14" "clang-toolchain@13"
      "python-lit"))))

(define base-tools
  (specifications->manifest
   '("python" "python-numpy" "python-numpy@1" "python-scipy"
     "python-scikit-learn"
     "python-scikit-fuzzy" "python-scikit-image" "python-scikit-rebate"
     "python-pytorch"

     "julia"
     "pari-gp"

     "openmpi@5" "openmpi@4" "openmpi-rocm"       ;MPI
     "mpich" "nmad" "hwloc"
     "intel-mpi-benchmarks"
     "osu-micro-benchmarks" "osu-micro-benchmarks-rocm"

     "gmp" "mpfr" "mpc"                           ;multiprecision

     "boost" "qtbase" "qttools" "googletest" "eigen" ;C++
     "openblas" "blis" "lapack" "gsl" "fftw"         ;linear algebra
     "petsc" "slepc" "petsc-openmpi" "slepc-openmpi"
     "python-petsc4py" "python-slepc4py"
     "sundials"
     "metis"
     "mumps" "mumps-metis" "mumps-metis-openmpi" "mumps-openmpi"
     "qr_mumps"

     "cmake" "make" "pkg-config"
     "autoconf" "automake" "libtool" "gettext" "texinfo"

     "slurm@23.11" "slurm@22"
     "slurm@20.11" "slurm@20.02"
     "irods"

     ;; Various utilities.
     "nano" "vim" "tmux" "screen" "htop" "git"
     "valgrind" "gdb" "recutils")))

(define (select-packages-by-location file-pattern)
  (fold-packages (lambda (package lst)
                   (cond ((package-superseded package)
                          lst)
                         ((not (package-location package))
                          lst)
                         ((string-contains (location-file
                                            (package-location package))
                                           file-pattern)
                          (cons package lst))
                         (else lst)))
                 '()))

(define hpc-tools
  ;; (packages->manifest (select-packages-by-location "inria/"))
  (specifications->manifest
   '("starpu" "starpu@1.3" "starpu-simgrid" "parcoach"
     "parsec" "quark"
     "topomatch"
     "chameleon" "chameleon-simgrid" "mini-chameleon" "pastix"
     "simgrid"
     "scotch" "scotch32" "pt-scotch" "pt-scotch32"
     "scotch:metis" "scotch32:metis" "pt-scotch:metis" "pt-scotch32:metis"
     "maphys" "maphys++"
     "mmg"
     "scalfmm"
     "eztrace" "fxt" "vite")))

(define non-free-tools
  ;; (packages->manifest (select-packages-by-location "non-free/"))
  (specifications->manifest
   '("cuda-toolkit@12" "cuda-toolkit@11" "cuda-toolkit@10"
     ;; "intel-compilers" ;FIXME: "source" tarball for 2020.0.x vanished
     "intel-oneapi-mkl"

     "openmpi-cuda" "osu-micro-benchmarks-cuda"
     "python-pytorch-with-cuda12"
     "starpu-cuda" "chameleon-cuda" "chameleon-mkl-mt")))

(concatenate-manifests
 (list toolchains base-tools hpc-tools non-free-tools))
