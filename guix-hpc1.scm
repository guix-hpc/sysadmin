;;; Configuration of the guix-hpc1 build node.

(use-modules (gnu))

(let ((base-os (load "build-node.scm")))
  (operating-system
    (inherit base-os)
    (host-name "guix-hpc1")

    ;; This is a non-EFI setup.
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (targets '("/dev/sda"))))

    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))

    (file-systems (cons* (file-system
                           (device (file-system-label "root"))
                           (mount-point "/")
                           (type "ext4"))
                         %base-file-systems))

    (swap-devices (list (swap-space (target "/dev/sda1"))))

    (services
     ;; Configure networking.
     (cons (service static-networking-service-type
                    (list (static-networking
                           (addresses (list (network-address
                                             (device "enp7s0f0")
                                             (value "194.199.1.19/27"))))
                           (routes (list (network-route
                                          (destination "default")
                                          (gateway "194.199.1.30"))))
                           (name-servers '("193.50.111.150")))))

           (operating-system-user-services base-os)))))
